<?php

namespace AzureSpring\Tnci\Model;

class OrderFilter extends Paginator
{
    /** @var array<int> */
    private $idVector = [];

    /** @var int|null */
    private $product;

    /** @var \DateTimeImmutable|null */
    private $from;

    /** @var \DateTimeImmutable|null */
    private $thru;

    public static function create()
    {
        return new OrderFilter();
    }

    /**
     * @return array
     */
    public function getIdVector(): array
    {
        return $this->idVector;
    }

    /**
     * @param array $idVector
     *
     * @return $this
     */
    public function setIdVector(array $idVector): self
    {
        $this->idVector = $idVector;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getProduct(): ?int
    {
        return $this->product;
    }

    /**
     * @param int|null $product
     *
     * @return $this
     */
    public function setProduct(?int $product): self
    {
        $this->product = $product;

        return $this;
    }

    /**
     * @return \DateTimeImmutable|null
     */
    public function getFrom(): ?\DateTimeImmutable
    {
        return $this->from;
    }

    /**
     * @param \DateTimeImmutable|null $from
     *
     * @return $this
     */
    public function setFrom(?\DateTimeImmutable $from): self
    {
        $this->from = $from;

        return $this;
    }

    /**
     * @return \DateTimeImmutable|null
     */
    public function getThru(): ?\DateTimeImmutable
    {
        return $this->thru;
    }

    /**
     * @param \DateTimeImmutable|null $thru
     *
     * @return $this
     */
    public function setThru(?\DateTimeImmutable $thru): self
    {
        $this->thru = $thru;

        return $this;
    }

    public function toParams(): array
    {
        return array_filter(parent::toParams() + [
            'item_id' => $this->getProduct(),
            'begin' => $this->getFrom() ? $this->getFrom()->getTimestamp() : null,
            'end' => $this->getThru() ? $this->getThru()->getTimestamp() : null,
            'orders_id' => implode(',', $this->getIdVector()),
        ]);
    }
}
