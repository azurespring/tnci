<?php

namespace AzureSpring\Tnci;

use AzureSpring\Tnci\Annotation\Template;
use AzureSpring\Tnci\Notification;
use AzureSpring\Tnci\Model\Order;
use AzureSpring\Tnci\Model\OrderFilter;
use AzureSpring\Tnci\Model\OrderOptions;
use AzureSpring\Tnci\Model\Paginated;
use AzureSpring\Tnci\Model\PaginatedResult;
use AzureSpring\Tnci\Model\Product;
use AzureSpring\Tnci\Model\ProductFilter;
use AzureSpring\Tnci\Model\Refund;
use AzureSpring\Tnci\Model\SingleResult;
use AzureSpring\Tnci\Serializer\TemplateHandler;
use GuzzleHttp\Client as Guzzle;
use JMS\Serializer\Handler\HandlerRegistryInterface;
use JMS\Serializer\SerializerBuilder;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\StreamFactoryInterface;


class Client
{
    private $guzzle;

    private $key;

    private $secret;

    private $serializer;

    private $contents;

    private $responseFactory;

    private $streamFactory;

    public function __construct(Guzzle $guzzle, string $key, string $secret, ResponseFactoryInterface $responseFactory, StreamFactoryInterface $streamFactory)
    {
        $this->guzzle = $guzzle;
        $this->key = $key;
        $this->secret = $secret;
        $this->serializer = SerializerBuilder::create()
            ->setMetadataDirs([
                'AzureSpring\\Tnci\\Model' => __DIR__.'/Resources/config/serializer',
            ])
            ->configureHandlers(function (HandlerRegistryInterface $registry) {
                $registry->registerSubscribingHandler(new TemplateHandler());
            })
            ->addDefaultHandlers()
            ->build()
        ;
        $this->responseFactory = $responseFactory;
        $this->streamFactory = $streamFactory;
    }

    public function findProducts(?ProductFilter $filter = null): Paginated
    {
        return $this->request(Template::bind(PaginatedResult::class, Product::class), 'item_list', ($filter ?? ProductFilter::create())->toParams());
    }

    public function findProduct(int $id): ?Product
    {
        $result = $this->findProducts(ProductFilter::create()->setIdVector([ $id ]));

        return current($result->getData());
    }

    public function findOrders(?OrderFilter $filter = null): Paginated
    {
        return $this->request(Template::bind(PaginatedResult::class, Order::class), 'orders_list', ($filter ?? OrderFilter::create())->toParams());
    }

    public function findOrder(int $id): ?Order
    {
        $result = $this->findOrders(OrderFilter::create()->setIdVector([ $id ]));

        return current($result->getData());
    }

    public function create(int $product, OrderOptions $options, int $quantity = 1): Order
    {
        return $this
            ->request(
                Template::bind(SingleResult::class, Order::class),
                'item_orders',
                [
                    'item_id' => $product,
                    'size' => $quantity,
                ] +
                $options->toParams()
            )
            ->getData()
        ;
    }

    public function update(string $id, OrderOptions $options, ?bool $send = null, ?bool $reset = null): Order
    {
        return $this
            ->request(
                Template::bind(SingleResult::class, Order::class),
                'item_orders_modify',
                [
                    'orders_id' => $id,
                    'sms_send' => [false => 0, true => 1, null => 2][$send],
                    're_code' => [false => 0, true => 1, null => 2][$reset],
                ] +
                $options->toParams()
            )
            ->getData()
        ;
    }

    public function remove(string $id, ?int $quantity = null, string $serialNo = ''): Refund
    {
        return $this
            ->request(
                Template::bind(SingleResult::class, Refund::class),
                'item_refund',
                array_filter([
                    'orders_id' => $id,
                    'size' => $quantity,
                    'serial_no' => $serialNo
                ])
            )
            ->getData()
        ;
    }

    public function getContents(): ?string
    {
        return $this->contents;
    }

    private function request(string $type, string $method, array $params = [])
    {
        $params['method'] = $method;
        $params['_pid'] = $this->key;
        $params['_sig'] = $this->sign($params);

        $response = $this->guzzle->post('', [ 'form_params' => $params ]);
        $this->contents = $response->getBody()->getContents();
        /** @var SingleResult|PaginatedResult $result */
        $result = $this->serializer->deserialize($this->contents, $type, 'json');
        if (!$result->isOk()) {
            throw new Exception($result->getMessage(), $result->getCode());
        }

        return $result;
    }

    private function sign(array $params)
    {
        $pv = array_map(
            function ($key, $value) {
                return "{$key}={$value}";
            },
            array_keys($params),
            array_values($params)
        );
        sort($pv, SORT_STRING);

        array_unshift($pv, $this->secret);
        array_push($pv, $this->secret);

        return strtoupper(md5(implode('&', $pv)));
    }

    public function recv(ServerRequestInterface $request)
    {
        $params = $request->getParsedBody();
        $sign = @$params['_sig'];
        unset($params['_sig']);
        if ($this->key !== $params['_pid'] || $this->sign($params) !== $sign) {
            return null;
        }

        /** @var Notification\AbstractNotification $class */
        foreach ([
                     Notification\RefundNotification::class,
                     Notification\VerificationNotification::class,
                 ] as $class) {
            if ($class::support($params)) {
                return $class::compose($params);
            }
        }

        return null;
    }

    public function ackn(): ResponseInterface
    {
        return $this
            ->responseFactory
            ->createResponse()
            ->withHeader('Content-Type', 'text/xml')
            ->withBody($this->streamFactory->createStream(
                $this->xmlize('root', ['success' => '1', 'message' => '成功'], true)
            ))
            ;
    }

    private function xmlize($name, $value, bool $prolog = false)
    {
        $document = new \DOMDocument('1.0', 'utf-8');
        $document->appendChild($element = $this->doXmlize($name, $value, $document));

        return $prolog ? $document->saveXML() : $document->saveXML($element);
    }

    private function doXmlize($name, $value, \DOMDocument $document)
    {
        if (!is_array($value)) {
            return $document->createElement($name, $value);
        }

        $element = $document->createElement($name);
        foreach ($value as $k => $v) {
            $element->appendChild($this->doXmlize($k, $v, $document));
        }

        return $element;
    }
}
